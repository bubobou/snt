# SNT

Cours de Sciences Numériques et Technologie

## Les thèmes

 - Internet
 - le Web
 - Les données structurées
 - L'informatique embarquée
 - Les réseaux sociaux
 - Localisation, cartographie et mobilité
 - La photographie numérique

## Thème transversal

 -Programmation