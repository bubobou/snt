# Le Web

# HTML et CSS

Nous allons nous intéresser à un acteur fondamental du développement web, le couple HTML+CSS (Hyper Text Markup Langage et Cascading Style Sheets).

Grâce au HTML vous allez pouvoir, dans votre navigateur (Firefox, Chrome, Opera,....), afficher du texte, afficher des images, proposer des hyperliens (liens vers d'autres pages web), afficher des formulaires et même maintenant afficher des vidéos (grâce à la dernière version du HTML, l'HTML5).

## TP : créer un mini-site Web

### Étape 1

!!! tip "A FAIRE : Créer une page web"

    À l'aide d'un éditeur de texte (par exemple **Notepad++**), créer un nouveau fichier.

    Sauvegardez-le en précisant son nom, par exemple **index.html**.

    Écrivez le code suivant dans votre éditeur de texte (sans oublier de sauvegarder quand vous avez terminé) :

    ```html
    <!doctype html>
    <html lang="fr">
        <head>
            <meta charset="utf-8">
            <title>Voici mon site</title>

        </head>
        <body>
            <h1>Hello World! Ceci est un titre</h1>
            <p>Ceci est un <strong>paragraphe</strong>. Avez-vous bien compris ?</p>

        </body>
    </html>    
    ```

Testez votre code à l'aide d'un navigateur web (Firefox ou Chrome) en “double-cliquant” sur le fichier index.html
***

Dans l'exemple précédent, vous reconnaissez le code se trouvant entre les balises ```<body>``` :

```
<body>
......
</body>
```

Tout votre code HTML devra se trouver entre ces 2 balises.

Le reste des balises devraient vous êtes inconnues. Passons-les en revue :

!!! note "les balises utilisées"

    La première ligne (```<!doctype html>```) permet d'indiquer au navigateur que nous utiliserons la dernière version du HTML, le fameux HTML5.

    La balise ```<html>``` est obligatoire, l'attribut lang="fr" permet d'indiquer au navigateur que nous utiliserons le français pour écrire notre page.

    Les balises ```<head>...</head>``` délimitent ce que l'on appelle l'en-tête. L'en-tête contient, dans notre exemple, 2 balises : la balise ```<meta charset="utf-8">``` qui permet de définir l'encodage des caractères (utf-8) et la balise ```<title>``` qui définit le titre de la page (attention ce titre ne s'affiche pas dans le navigateur, ne pas confondre avec la balise ```<h1>```).

###  Étape 2

!!! tip "A FAIRE : Créer une feuille de style"
    Toujours à l'aide d'un éditeur de texte, vous allez créer un fichier qui va contenir le CSS de notre page (par exemple style.css). Complétez ce fichier à l'aide du code suivant :

    ```css
    h1
    {
    text-align: center;
    background-color: red;
    font-family: Verdana;
    }
    p
    {
    font-family: Verdana;
    font-style: italic;
    color: green;
    }
    ```


!!! tip "A FAIRE : Relier le style à la page web"
    Pour l'instant notre CSS ne sera pas appliqué à notre page, pour ce faire, il faut modifier notre code HTML en ajoutant une ligne qui va permettre d'associer notre code CSS à notre page.

    Modifiez le code de la page *index.html* avec la ligne suivante ```<link rel="stylesheet" href="style.css">``` en la plaçant comme ci-dessous :  

    ```html  
       <!doctype html>
        <html lang="fr">
        <head>
            <meta charset="utf-8">
            <title>Voici mon site</title>
            <link rel="stylesheet" href="style.css">
        </head>
        <body>
            <h1>Hello World! Ceci est un titre</h1>
            <p>Ceci est un <strong>paragraphe</strong>. Avez-vous bien compris ?</p>
        </body>
        </html>
     ```  

    ```html
    <body>
        <h1>Hello World! Ceci est un titre</h1>
        <p>Ceci est un <strong>paragraphe</strong>. Avez-vous bien compris ?</p>
    </body>
    ```

Testez votre code à l'aide d'un navigateur web en “rafraichissant” la page *index.html*.
***

###  Étape 3

Dans l'exemple que nous venons de voir, les fichiers "index.html" et "style.css" se trouvent dans le même dossier. Il est souvent utile de placer les fichiers CSS dans un dossier “CSS”. 

!!! tip "A FAIRE : Déplacer le fichier *style.css*"
    Dans l'explorateur de fichier il faut :  

    - ouvrir le dossier où sont enregistrer *index.html* et *style.css*,  
    - créer un nouveau dossier et le nommer "CSS",  
    - déplacer le fichier *style.css* dans ce dossier,  
    - modifier dans le code de la page *index.html* le lien vers la feuille de style :      
    ```html
    <link rel="stylesheet" href="style.css">
    ```
    devient :  
    ```html
    <link rel="stylesheet" href="CSS/style.css">
    ```
### Balises utiles



!!! tldr "Voici quelques balises très utilisées :"

    === "`<a>`"

        La balise de l'**hypertexte** !

        ```html
        <a href="mon_autre_page.html">Cliquez ici pour vous rendre sur mon autre page</a>
        ```

        La balise a permet de créer des liens hypertextes, ce sont ces liens hypertextes qui vous 
        permettent de "voyager" entre les pages d'un site ou entre les sites. Les liens hypertextes
        sont par défaut soulignés et de couleur bleue (modifiable grâce au CSS).
        
        La balise a possède un attribut `href` qui a pour valeur le chemin du fichier que l'on cherche 
        à atteindre ou l'adresse du site cible 
        (exemple : ```<a href="https://www.atrium-sud.fr">Cliquez ici pour vous rendre sur Atrium.fr</a>```). 
        Entre la balise ouvrante et fermante, on trouve le texte qui s'affichera à l'écran 
        c'est ce texte qui est souligné et de couleur bleue).
        
        La balise peut sans problème se trouver en plein milieu d'un paragraphe.

    === "`<img/>`"

        Comme vous devez déjà vous en douter, la balise <img> sert à insérer des ... images :

        ```html
        <img src="mon_image.jpg" alt="avion"/>
        ```

        la balise `img` est à la fois ouvrante et fermante comme la balise `br`. Elle possède 2 attributs :

        - *src* qui doit être égal au nom du fichier image (ou au chemin si le fichier image se trouve dans un autre dossier).  
        - *alt* qui doit être égal à une description de votre image (cet attribut est utilisé notamment par les systèmes 
         de description des pages web utilisées par les non-voyants), il faut donc systématiquement renseigner cet attribut.

    === "`<form>`, `<input>` et `<button>`"

        Les formulaires sont des éléments importants des sites internet, ils permettent à l'utilisateur de transmettre des informations. Un formulaire devra être délimité par une balise form (même si ce n'est pas une obligation) :

        ```
        <form>
        ....
        </form>
        ```

        Il existe différentes balises permettant de construire un formulaire, notamment la balise `input`. 
        Cette balise possède un attribut type qui lui permet de jouer des rôles très différents.

        La balise `button` nous sera aussi d'une grande utilité.

###  Étape 4

!!! tip "A FAIRE : Créer une deuxième page web"
    Créez un fichier html contenant le code suivant :

    Sauvegardez-le sous le nom : **formulaire.html**.

    ```html
    <!doctype html>
    <html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>Voici mon site</title>

    </head>
    <body>
        <form>
            <h1>Quelques exemples de balises de formulaire</h1>
            <p>voici un champ de texte : <input type="text"/></p>
            <p>voici une checkbox <input type="checkbox"/></p>
            <button>Cliquez ici !</button>
        </form>
    </body>
    </html>
    ```

!!! tip "A FAIRE : Lier les pages"
    Dans cette nouvelle page, ajouter un lien vers la page *index.html* en utilisant une balise `<a>` telle qu'expliquée plus haut.

    De même dans la page *index.html*, insérer un lien vers la page *formulaire.html*.

    N'oublier pas non plus d'ajouter la ligne code permettant d'appliquer notre feuille de style à cette nouvelle page web.

