# Notions transversales de programmation



| Contenus | Capacités |
|---|---|
| - Affectations, variables <br> - Séquences   <br>   - Instructions conditionnelles   <br> - Boucles bornées et non bornées <br>  - Définitions et appels de fonctions    &nbsp;&nbsp;&nbsp;&nbsp; | Écrire et développer des programmes pour répondre à des problèmes et modéliser des phénomènes physiques, économiques et sociaux.|


---

# Internet

| Contenus | Capacités |
|---|---|
|Protocole TCP/IP : paquets, routage des paquets| Distinguer le rôle des protocoles IP et TCP.    <br>  Caractériser les principes du routage et seslimites.<br>  Distinguer la fiabilité de transmission et l’absence de garantie temporelle.|
|Adresses symboliques et serveurs DNS|Sur des exemples réels, retrouver une adresse IP à partir d’une adresse symbolique et inversement.|
|Réseaux pair-à-pair| Décrire l’intérêt des réseaux pair-à-pair ainsi que les usages illicites qu’on peut en faire.| 
|Indépendance d’internet rapport au réseau physique |Caractériser quelques types de réseaux physiques : obsolètes <br>  Caractériser l’ordre de grandeur du trafic de données sur internet et son évolution.|

# Le Web

| Contenus | Capacités |
|---|---|
|Repères historiques|Connaître les étapes du développement du Web.|
|Notions juridiques|Connaître certaines notions juridiques (licence, droit d’auteur, droit d’usage, valeur d’un bien).|
|Hypertexte | Maîtriser les renvois d’un texte à différents contenus.|
|Langages HTML et CSS | Distinguer ce qui relève du contenu d’une page et de son style de présentation. <br> Étudier et modifier une page HTML simple.|
|URL|Décomposer l’URL d’une page.<br> Reconnaître les pages sécurisées.|
|Requête HTTP | Décomposer le contenu d’une requête HTTP et identifier les paramètres passés.|
|Modèle client/serveur | Inspecter le code d’une page hébergée par un serveur et distinguer ce qui est exécuté par le client et par le serveur.|
|Moteurs de recherche : principes et usages | Mener une analyse critique des résultats fournis par un moteur de recherche. <br> Comprendre les enjeux de la publication d’informations.|
|Paramètres de sécurité d’un navigateur.| Maîtriser les réglages les plus importants concernant la gestion des cookies, la sécurité et la confidentialité d’un navigateur <br> Sécuriser sa navigation en ligne et analyser les pages et fichiers.|

# Les réseaux sociaux

| Contenus | Capacités |
|---|---|
|Identité numérique, e-réputation, identification, authentification | Connaître les principaux concepts liés à l’usage des réseaux sociaux.|
|Réseaux sociaux existants | Distinguer plusieurs réseaux sociaux selon leurs caractéristiques, y compris un ordre de grandeur de leurs nombres d’abonnés.<br> Paramétrer des abonnements pour assurer la confidentialitéde données personnelles.|
|Modèle économique des réseaux sociaux | Identifier les sources de revenus des entreprises de réseautage social.|
|Rayon, diamètre et centre d’un graphe | Déterminer ces caractéristiques sur des graphes simples.|
|Notion de « petit monde » <br> Expérience de Milgram | Décrire comment l’information présentée par les réseaux sociaux est conditionnée par le choix préalable de ses amis.
|Cyberviolence|Connaître les dispositions de l’article 222-33-2-2 du codepénal.<br> Connaître les différentes formes de cyberviolence (harcèlement, discrimination, sexting...) et les ressources disponibles pour lutter contre la cyberviolence.|


# Les données structurées et leur traitement

| Contenus | Capacités |
|---|---|
|Données |Définir une donnée personnelle. <br> Identifier les principaux formats et représentations de données.|
|Données structurées | Identifier les différents descripteurs d’un objet.<br> Distinguer la valeur d’une donnée de son descripteur.<br> Utiliser un site de données ouvertes, pour sélectionner et récupérer des données.|
|Traitement de données structurées | Réaliser des opérations de recherche, filtre, tri ou calcul sur une ou plusieurs tables.|
|Métadonnées |Retrouver les métadonnées d’un fichier personnel.|
|Données dans le nuage (cloud)|Utiliser un support de stockage dans le nuage.<br>Partager des fichiers, paramétrer des modes de synchronisation.<br>Identifier les principales causes de la consommation énergétique des centres de données ainsi que leur ordre de grandeur.|

---

# Localisation, cartographie et mobilité

| Contenus | Capacités |
|---|---|
|GPS, Galileo|Décrire le principe de fonctionnement de la géolocalisation.
|Cartes numériques|Identifier les différentes couches d’information de GeoPortail pour extraire différents types de données.<br> Contribuer à OpenStreetMap de façon collaborative.|
|Protocole NMEA 0183|Décoder une trame NMEA pour trouver des coordonnées géographiques.|
|Calculs d’itinéraires|Utiliser un logiciel pour calculer un itinéraire.<br>Représenter un calcul d’itinéraire comme un problème sur un graphe.|
|Confidentialité|Régler les paramètres de confidentialité d’un téléphone pour partager ou non sa position.|

---

# Informatique embarquée et objets connectés

| Contenus | Capacités |
|---|---|
|Systèmes informatiques embarqués|Identifier des algorithmes de contrôle des comportements physiques à travers les données des capteurs, l’IHM et les actions des actionneurs dans des systèmes courants.|
|Interface homme-machine|(IHM)Réaliser une IHM simple d’un objet connecté.|
|Commande d’un actionneur,acquisition des données d’un capteur|Écrire des programmes simples d’acquisition de données ou de commande d’un actionneur.|

# La photographie numérique   

| Contenus | Capacités | 
|---|---|
|Photosites, pixels, résolution (du capteur, de l’image), profondeur de couleur| Distinguer les photosites du capteur et les pixels de l’image en comparant les résolutions du capteur et de l’image selon les  réglages de l’appareil. | 
| Métadonnées EXIF | Retrouver les métadonnées d’une photographie.|
| Traitement d’image | Traiter par programme une image pour la transformer en agissant sur les trois composantes de ses pixels.|
| Rôle des algorithmes dans les appareils photo numériques | Expliciter des algorithmes associés à la prise de vue.<br> Identifier les étapes de la construction de l’image finale.|

